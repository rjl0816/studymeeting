//
//  ContentView.swift
//  StudyMeeting
//
//  Created by Lotino Ron Jefferson on 2020/08/27.
//  Copyright © 2020 Lotino Ron Jefferson. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
